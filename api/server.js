const express = require('express');
const cors = require('cors');
const news = require('./app/news');
const comments = require('./app/comments');
const mysql = require('mysql');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static('public'));


const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'user',
  password : '1qaz@WSX29',
  database : 'exam_posts'
});

const port = 8000;

app.use('/news', news(connection));
app.use('/comments', comments(connection));

connection.connect((err) => {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }

  console.log('connected as id ' + connection.threadId);

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});

