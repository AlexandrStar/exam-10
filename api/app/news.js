const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = connection => {

  const router = express.Router();

  router.get('/', (req, res) => {
    connection.query('SELECT * FROM `news`', (error,results) =>{
      if (error){
        res.status(500).send({error:'Database error'});
      }
      res.send(results);
    });

  });

  router.get('/:id', (req, res) => {
    connection.query('SELECT * FROM `news` WHERE `id` = ?',req.params.id, (error,results) =>{
      if (error){
        res.status(500).send({error:'Database error'});
      }
      res.send(results);
    });
  });

  router.post('/', upload.single('image'), (req, res) => {
    const news = req.body;

    const dateTime = new Date().toISOString();
    news.datetime = dateTime;

    if (req.file) {
      news.image = req.file.filename;
    }

    connection.query('INSERT INTO `news` (`title`, `content`, `image`, `datetime`) VALUES (?, ?, ?, ?)',
      [news.title, news.content, news.image, news.datetime],
      (error, results) => {
        if (error){
          console.log(error);
          res.status(500).send({error:'Database error'});
        }

        res.send({message: 'OK'});
      }
    );
  });

  router.delete('/:id', (req, res) => {
    connection.query('DELETE FROM `news` WHERE `id` = ?',req.params.id, (error,results) =>{
      if (error){
        res.status(500).send({error:'Database error'});
      }
      res.send({message: 'OK'});
    });
  });

  return router;
};

module.exports = createRouter;