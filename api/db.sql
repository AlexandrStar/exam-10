CREATE SCHEMA `exam_posts` DEFAULT CHARACTER SET utf8 ;

USE `exam_posts`;

CREATE TABLE `news` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `content` VARCHAR(255) NOT NULL,
  `image` VARCHAR(100) NULL,
  `datetime` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`));

  CREATE TABLE `comments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `news_id` INT NULL,
  `author` VARCHAR(255) NULL,
  `comment` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `news_id_fk_idx` (`news_id` ASC),
  CONSTRAINT `news_id_fk`
    FOREIGN KEY (`news_id`)
    REFERENCES `news` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

INSERT INTO `news` (`id`, `title`, `content`, `image`, `datetime`)
VALUES
	(1, 'Lorem ipsum dolor sit amet.', 'Lorem ipsum dolor sit amet, elit. A, officiis.', NULL, '25.03.2019'),
	(2, 'Lorem ipsum dolor sit.', 'Lorem ipsum dolor sit amet, consectetur. A, officiis.', NULL, '26.03.2019'),
	(3, 'Lorem ipsum dolor.', 'Lorem ipsum dolor sit amet, consectetur adipisicing.', NULL, '27.03.2019'),
	(4, 'Lorem ipsum.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.', NULL, '28.03.2019'),
	(5, 'Lorem.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, officiis.', NULL, '29.03.2019');

INSERT INTO `comments` (`id`, `news_id`, `author`, `comment`)
VALUES
	(1, 1, 'Vasya', 'Good job'),
	(2, 2, NULL, 'Good news'),
	(3, 3, 'Petya', 'Good job'),
	(4, 4, NULL, 'LOL'),
	(5, 5, 'John', 'Good job');