import React from 'react';

import imageNotAvailable from '../../assets/images/image_not_available.png';
import {apiURL} from "../../constans";

const style = {
  width: '100px',
  height: '100px',
  marginRight: '10px'
};

const PostsThumbnail = props => {
  let image = imageNotAvailable;

  if (props.image) {
    image = apiURL + '/uploads/' + props.image;
  }

  return <img src={image} style={style} className="img-thumbnail" alt="Product" />;
};

export default PostsThumbnail;