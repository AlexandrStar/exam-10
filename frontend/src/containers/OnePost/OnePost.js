import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody, Col, Form, FormGroup, Input, Label} from "reactstrap";
import PostsThumbnail from "../../components/PostsThumbnail/PostsThumbnail";
import {Link} from "react-router-dom";
import {createComment, fetchComments, fetchOnePost, removeComment} from "../../store/actions/actionsType";
import {connect} from "react-redux";

class OnePost extends Component {
  state = {
    author: '',
    comment: '',
  };

  componentDidMount() {
    this.props.onFetchOnePost(this.props.match.params.id);
    this.props.onFetchComments(this.props.match.params.id);

  }

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  render() {
    console.log('Один пост',this.props.posts[0]);
    console.log('Коментарий к данному посту', this.props.comments[0]);
    console.log('Коментарии', this.props.comments);
    return (
      <Fragment>

        <Card style={{marginTop: '10px'}}>
          <CardBody>
            <PostsThumbnail image={this.props.posts.image} />
            <h3>
              {this.props.posts[0] && this.props.posts[0].title}
            </h3>
            <p style={{marginLeft: '10px', color: 'light grey'}}>
              {this.props.posts[0] && this.props.posts[0].datetime}
            </p>
            <strong style={{marginLeft: '10px'}}>
              {this.props.posts[0] && this.props.posts[0].content}
            </strong>
          </CardBody>
        </Card>

        <h3>Comments</h3>
        {this.props.comments.map(comment => (
          <Card key={comment.id} style={{marginTop: '10px'}}>
            <CardBody>
              <h3>
                {!comment.author ? <span>Anonymous</span>: comment.author}
              </h3>
              <strong style={{marginLeft: '10px'}}>
                {comment.comment}
              </strong>
              <Button onClick={() => this.props.removeComment(comment.id)}>
                Delete
              </Button>
            </CardBody>
          </Card>
        ))}


        <h3>Add comment</h3>
      <Form onSubmit={this.submitFormHandler}>
        <FormGroup row>
          <Label sm={2} for="author">Name</Label>
          <Col sm={10}>
            <Input
              type="text" required
              name="author" id="author"
              placeholder="Enter name"
              value={this.state.author}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label sm={2} for="comment">Comment</Label>
          <Col sm={10}>
            <Input
              type="textarea" required
              name="comment" id="comment"
              placeholder="Enter comment"
              value={this.state.comment}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col sm={{offset:2, size: 10}}>
            <Button type="submit" color="primary">Add</Button>
          </Col>
        </FormGroup>
      </Form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts.posts,
  comments: state.posts.comments,
});

const mapDispatchToProps = dispatch => ({
  onFetchOnePost: id => dispatch(fetchOnePost(id)),
  onFetchComments: id => dispatch(fetchComments(id)),
  onCommentCreated: commentData => dispatch(createComment(commentData)),
  removeComment: id => dispatch(removeComment(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(OnePost);