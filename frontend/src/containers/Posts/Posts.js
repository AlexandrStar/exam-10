import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody} from "reactstrap";
import {fetchPosts, removePost} from "../../store/actions/actionsType";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import PostsThumbnail from "../../components/PostsThumbnail/PostsThumbnail";

class Posts extends Component {

  componentDidMount() {
    this.props.onFetchPosts();
  }

  render() {
    return (
      <Fragment>
        <h2>
          Posts
          <Link to="/news/new">
            <Button
              color="primary"
              className="float-right"
              >
              Add post
            </Button>
          </Link>
        </h2>

        {this.props.posts.map(post => (
          <Card key={post.id} style={{marginTop: '10px'}}>
            <CardBody>
              <PostsThumbnail image={post.image} />
              <strong style={{marginLeft: '10px'}}>
                {post.title}
              </strong>
              <p style={{marginLeft: '10px', color: 'light grey'}}>
                {post.datetime}
              </p>
              <Link to={'/news/' + post.id}>
                Read Full Post >>
              </Link>
              <Button onClick={() => this.props.removePost(post.id)}>
                Delete
              </Button>
            </CardBody>
          </Card>
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts.posts,
});

const mapDispatchToProps = dispatch => ({
  onFetchPosts: () => dispatch(fetchPosts()),
  removePost: id => dispatch(removePost(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Posts);