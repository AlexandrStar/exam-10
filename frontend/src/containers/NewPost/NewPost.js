import React, {Component, Fragment} from 'react';
import PostsForm from "../../components/PostsForm/PostsForm";
import {createPost} from "../../store/actions/actionsType";
import {connect} from "react-redux";

class NewPost extends Component {
  createPost = postData => {
    this.props.onPostCreated(postData).then(() => {
      this.props.history.push('/')
    })
  };
  render() {
    return (
      <Fragment>
        <h2>New post</h2>
        <PostsForm onSubmit={this.createPost} />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onPostCreated: postData => dispatch(createPost(postData))
});

export default connect(null, mapDispatchToProps)(NewPost);