import React, {Component, Fragment} from 'react';
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {Container} from "reactstrap";
import {Route, Switch} from "react-router-dom";
import Products from "./containers/Posts/Posts";
import NewProduct from "./containers/NewPost/NewPost";
import OnePost from "./containers/OnePost/OnePost";

class App extends Component {
  render() {
    return (
      <Fragment>
        <header>
          <Toolbar/>
        </header>
        <Container>
          <Switch>
            <Route path="/" exact component={Products}/>
            <Route path="/news/new" exact component={NewProduct}/>
            <Route path="/news/:id" exact component={OnePost}/>
          </Switch>
        </Container>
      </Fragment>
    );
  }
}

export default App;
