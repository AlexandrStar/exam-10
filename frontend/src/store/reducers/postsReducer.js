import {FETCH_COMMENTS_SUCCESS, FETCH_POSTS_SUCCESS} from "../actions/actionsType";

const initialState = {
  posts: [],
  comments: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POSTS_SUCCESS:
      return {...state, posts: action.posts};
    case FETCH_COMMENTS_SUCCESS:
      return {...state, comments: action.comments};
    default:
      return state;
  }
};

export default reducer;