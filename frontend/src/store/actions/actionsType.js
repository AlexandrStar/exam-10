import axios from '../../axios-api';

export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const CREATE_COMMENTS_SUCCESS = 'CREATE_COMMENTS_SUCCESS';
export const REMOVE_POST_SUCCESS = 'REMOVE_POST_SUCCESS';

export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, comments});
export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});
export const createCommentSuccess = () => ({type: CREATE_COMMENTS_SUCCESS});
export const removePostSuccess = () => ({type: REMOVE_POST_SUCCESS});

export const fetchPosts = () => {
  return dispatch => {
    return axios.get('/news').then(
      response => dispatch(fetchPostsSuccess(response.data))
    );
  };
};

export const fetchOnePost = id => {
  return dispatch => {
    return axios.get(`/news/${id}`).then(
      response => {dispatch(fetchPostsSuccess(response.data))}
    );
  };
};

export const createPost = postData => {
  return dispatch => {
    return axios.post('/news', postData).then(
      () => dispatch(createPostSuccess())
    );
  };
};

export const removePost = id => {
  return dispatch => {
    return axios.delete(`/news/${id}`).then(
      response => {dispatch(fetchPostsSuccess())}
    );
  };
};

export const fetchComments = id => {
  return dispatch => {
    return axios.get(`/comments/${id}`).then(
      response => dispatch(fetchCommentsSuccess(response.data))
    );
  };
};

export const createComment = commentData => {
  return dispatch => {
    return axios.post('/comments', commentData).then(
      () => dispatch(createCommentSuccess())
    );
  };
};

export const removeComment = id => {
  return dispatch => {
    return axios.delete(`/news/${id}`).then(
      response => {dispatch(fetchCommentsSuccess())}
    );
  };
};